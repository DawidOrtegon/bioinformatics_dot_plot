% Read and sort the sequences existing in the fasta file. 

clear all; 
close all; 

file1a = 'sequence.fasta';
file2a = strrep(file1a,'fasta', 'txt'); 
copyfile(file1a,file2a)
fidp = fopen(file2a); 
C = textscan(fidp, '%s', 'delimiter', '\n');
filedata = readtable('SecuenciaPrueba.txt','delimiter','\n');
identificator = C{1}{1}; 
sequence = strcat(C{1}{2:size(C{1,1}, 1),:});  

% Creation of the first structure for the first sequence. 
sa = struct('Identificator', identificator, 'Sequence', sequence);
a = sa.Sequence; 
celldisp(C);


file1b = 'sequence(1).fasta';
file2b = strrep(file1b,'fasta', 'txt'); 
copyfile(file1b,file2b)
fidpb = fopen(file2b); 
C2 = textscan(fidpb, '%s', 'delimiter', '\n');
filedata2 = readtable('SecuenciaPrueba.txt','delimiter','\n');
identificator2 = C2{1}{1}; 
sequence2 = strcat(C2{1}{2:size(C2{1,1}, 1),:});  

% Creation of the first structure for the second sequence. 
sa2 = struct('Identificator', identificator2, 'Sequence', sequence2);
celldisp(C2);
b = sa2.Sequence; 

% Make both sequences the same lenght to avoid dimensional problems. 
maxLength = max(length(a), length(b)); 
a(end+1:maxLength) = 0;
b(end+1:maxLength) = 0;

