% Example for the comparassion and plot. 
close all 
clear all 

% Selection for the sequences in fasta format. 
[files]=uigetfile({'*.fasta;','All Fasta Files'},'Select the Files', 'Multiselect', 'on');

file1a = files{1,1};
file2a = strrep(file1a,'fasta', 'txt'); 
% The files has to be in the main directory of Matlab. 
copyfile(file1a,file2a)
fidp = fopen(file2a); 
Cab = textscan(fidp, '%s', 'delimiter', '\n');
filedata = readtable('SecuenciaPrueba.txt','delimiter','\n');
identificator = Cab{1}{1}; 
sequence = strcat(Cab{1}{2:size(Cab{1,1}, 1),:});  
sa = struct('Identificator', identificator, 'Sequence', sequence);
a = sa.Sequence; 
celldisp(Cab);


file1b = files{1,2}; 
file2b = strrep(file1b,'fasta', 'txt'); 
copyfile(file1b,file2b)
fidpb = fopen(file2b); 
Cab2 = textscan(fidpb, '%s', 'delimiter', '\n');
filedata2 = readtable('SecuenciaPrueba.txt','delimiter','\n');
identificator2 = Cab2{1}{1}; 
sequence2 = strcat(Cab2{1}{2:size(Cab2{1,1}, 1),:});  
sa2 = struct('Identificator', identificator2, 'Sequence', sequence2);
celldisp(Cab2);
b = sa2.Sequence; 

% Portion to make the two sequences equal. 
La = length(a); 
Lb = length(b); 
maxLength = max(La, Lb); 
a(end+1:maxLength) = 0;
b(end+1:maxLength) = 0;

% Alternative sequences to make the easiest dot plot. 
% a = 'ATGCGATGGTTATTTTCTACCAATCATAAAGATATTGGGACATTATATATTTTATTTGGGATATGATCTG'; 
% b = 'GGTTGGTTGGAACTGCTCTTAGATTGTTAATTCGTGCAGAATTAGGTCAACCGGGTGCCTTGCTTGGAGA';  
m = zeros(length(a),length(b)); 
La = length(a); 
Lb = length(b); 

% Variable that describes the size of the window. 
v = input('Write the cuadratic dimension for the filtering window '); 
% Variable that describes the minimum amount of coincidences in the window. 
e = input('Write the thereshold of coincidences that has to be shown in the window '); 

 % Comparassion from 1 base to many bases. 
 tic
 for k = 1:length(a)
     m(k,:) = eq(a(k),b(:));
 end 
toc 
 % Elimination of 0, to not be in the final plot. 
     for i = 1:length(m(:,1))
         for j = 1:length(m(1,:))
            if (m(i,j) == 0)
                m(i,j) = NaN; 
            end
         end
     end
     
  % Reescalation of the matrix in order to get each row in an individual
  % row of the plot. 
  
  n = (1:length(m))'; 
  m2 = double(m).*n;
  
% Total plot for the comparassion of the sequences.   
tic
hold(axes('Parent',figure,'XAxisLocation','top'));
axis ij; 
for s = 1:length(m2(1,:))
     plot(m2(s,:), 'sb');
     hold on
     xlabel('Sequence 1'); 
     ylabel('Sequence 2'); 
     xlim([1 La]); 
     ylim([1 Lb]); 
     
     % Plot of the one to one base compare. 
     if (strcmp (a(s), b(s)) == 1)
        C(s) = s; 
        plot(C,'sr'); 
    end 
     
end
toc

% Image for all the coincidences comparing all  between the sequences. 
hold(axes('Parent',figure,'XAxisLocation','top'));
axis ij; 
imagesc(m)
xlim([1 La]);
ylim([1 Lb]); 
xlabel('Sequence 1'); 
ylabel('Sequence 2'); 


%% Making of the vectors to divide the principal matrix in windows. 
 for h = 1:floor(length(m2)/v)
Vector1(h) = v; 
Vector2(h) = v; 
 end
 
 L = length(m2); 
 if (sum(Vector1) && sum(Vector2) ~= L)
    Vector1(end) = v + (L - sum(Vector1));
    Vector2(end) = v + (L- sum(Vector2)); 
 end

% Declaration of the matrix with each window. 
N = mat2cell(m2,Vector1, Vector2); 

% Conditioning and ploting of each of the submatrices. If in the principal
% diagonal is an e numer of coincidences, please plot the matrix. 



for i = 1:length(N)
    for j = 1:length(N)
    Diagonal = diag(N{i,j}); 
        if(sum(~isnan(Diagonal)) >= 0 && (numel(Diagonal)) >= (v-e))
        % figure; 
        % hold on;
        cellfun(@plot, N)
        end
    end 
end 

%-- ALTERNATIVE FROM 

% nl = v-1; 
% m3 = zeros(Lb,La); 
% 
% for i = 1:(Lb - nl)
%     for j = 1:(La - nl)
%         x1 = m2(1:(i+nl), 1:(j+nl)); 
%         Diagonal = diag(x1); 
%             if sum(Diagonal) >= (v-e)
%                 for K = 0:(length(x1)-1)
%                     m3(i+k, j+k) = x1(k+1, k+1);
%                 end
%             end 
%     end 
% end 
% 
% spy(m3)
% delete(findall(findall(gcf, 'Type', 'axe'), 'Type', 'text'))

%-- END ALTERNATIVE FROM 

Finish = true; 