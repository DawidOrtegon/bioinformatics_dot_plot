% Example for the comparassion and plot. 
close all 
clear all 

% Selection for the sequences in fasta format. 
[files]=uigetfile({'*.fasta;','All Fasta Files'},'Select the Files', 'Multiselect', 'on');

file1a = files{1,1};
file2a = strrep(file1a,'fasta', 'txt'); 

% The files has to be in the main directory of Matlab. 
copyfile(file1a,file2a)
fidp = fopen(file2a); 
Cab = textscan(fidp, '%s', 'delimiter', '\n');
identificator = Cab{1}{1}; 
sequence = strcat(Cab{1}{2:size(Cab{1,1}, 1),:});  
sa = struct('Identificator', identificator, 'Sequence', sequence);
a = sa.Sequence; 


file1b = files{1,2}; 
file2b = strrep(file1b,'fasta', 'txt'); 
copyfile(file1b,file2b)
fidpb = fopen(file2b); 
Cab2 = textscan(fidpb, '%s', 'delimiter', '\n');
identificator2 = Cab2{1}{1}; 
sequence2 = strcat(Cab2{1}{2:size(Cab2{1,1}, 1),:});  
sa2 = struct('Identificator', identificator2, 'Sequence', sequence2);
b = sa2.Sequence; 

% Portion to make the two sequences equal. 
La = length(a); 
Lb = length(b); 
maxLength = max(La, Lb); 
a(end+1:maxLength) = 0;
b(end+1:maxLength) = 0;

% % Alternative sequences to make the easiest dot plot. 
% a = 'GCAACAGTCCTAACATTCACCTCTCGTGTGTTTGTGTCTGTTCGCCATCCCGTCTCCGCTCG'; 
% b = 'AGCAGTAACTTCCTAAGCTTACCTCCATCTCTCCCCCGCCCTTCTTGCTGGTTCTCCGTCAG'; 
% % La = length(a); 
% % Lb = length(b); 
% 
% % Portion to make the two sequences equal. 
% La = length(a); 
% Lb = length(b); 
% maxLength = max(La, Lb);
% a(end+1:maxLength) = 0;
% b(end+1:maxLength) = 0;

% Variable that describes the size of the window. 
v = input('Write the quadratic dimension for the filtering window '); 
% Variable that describes the minimum amount of coincidences in the window. 
e = input('Write the threshold of coincidences that has to be shown in the window '); 

 % Comparassion from 1 base to many bases. 
 tic
 m = zeros(length(a), length(b)); 
 for k = 1:length(a)
     m(k,:) = eq(a(k),b(:));
 end 
toc 
 % Elimination of 0, to not be in the final plot. 
     for i = 1:length(m(:,1))
         for j = 1:length(m(1,:))
            if (m(i,j) == 0)
                m(i,j) = NaN; 
            end
         end
     end
     
  % Reescalation of the matrix in order to get each row in an individual
  % row of the plot. 
  
  n = (1:length(m))'; 
  m2 = double(m).*n;
  
% Total plot for the comparassion of the sequences.   
tic
hold(axes('Parent',figure,'XAxisLocation','top'));
axis ij; 
for s = 1:length(m2(1,:))
     plot(m2(s,:), 'sb');
     hold on
     x01=10;
     y01=10;
     width1=600;
     height1=600; 
     set(gcf,'position',[x01,y01,width1,height1])
     xlabel('Sequence 1'); 
     ylabel('Sequence 2'); 
     xlim([1 La]); 
     ylim([1 Lb]); 
     
     % Plot of the one to one base compare. 
     if (strcmp (a(s), b(s)) == 1)
        C(s) = s; 
        plot(C,'sr'); 
    end 
     
end
% Save the figure in the format given. 
saveas(gcf,'Dot_Plot_Complete.png'); 
toc

% Image for all the coincidences comparing all  between the sequences. 
hold(axes('Parent',figure,'XAxisLocation','top'));
axis ij; 
imagesc(m)
x02=10;
y02=10;
width2=600;
height2=600; 
set(gcf,'position',[x02,y02,width2,height2])
xlim([1 length(a)]);
ylim([1 length(b)]); 
xlabel('Sequence 1'); 
ylabel('Sequence 2'); 
saveas(gcf,'Dot_Plot_Image.png')


%% Making of the vectors to divide the principal matrix in windows or cells. 

%  for h = 1:floor(length(m2)/v)
% Vector1(h) = v; 
% Vector2(h) = v; 
%  end
%  
%  % Put at the end of the sequence more digits in case the dimension to make
%  % the cell is not complete. 
%  L = length(m2); 
%  if (sum(Vector1) && sum(Vector2) ~= L)
%     Vector1(end) = v + (L - sum(Vector1));
%     Vector2(end) = v + (L- sum(Vector2)); 
%  end
% 
% % Declaration of the matrix with each window. 
% N = mat2cell(m2,Vector1, Vector2); 
% NF = cell(length(N)); 
% Conditioning and ploting of each of the submatrices. If in the principal
% diagonal is an e numer of coincidences, please plot the matrix. 
% 
% tic 
% for i = 1:length(N)
%     for j = 1:length(N)
%     Diagonal = diag(N{i,j}); 
%         if(sum(~isnan(Diagonal)) >= 0 && (numel(Diagonal)) >= (v-e))
%             NF{i,j} = N{i,j}; 
%             figure; 
%             hold on; 
%             cellfun(@plot, NF)
%         end
%     end 
% end 
% toc


%% -- ALTERNATIVE FROM 
tic
nl = v-1; 
m3 = zeros(length(a),length(b)); 

for i = 1:(length(b) - nl)
   for j = 1:(length(a)-nl)
        x1 = m2(i:i+nl, j:j+nl); 
        if sum(diag(x1)) >= v-e
            for k = 0:length(x1)-1
                m3(i+k, j+k) = x1(k+1, k+1); 
            end 
        end 
    end 
end 
 
hold(axes('Parent',figure,'XAxisLocation','top'));
axis ij; 
spy(m3)
delete(findall(findall(gcf, 'Type', 'axe'), 'Type', 'text')); 
x03=10;
y03=10;
width3=600;
height3=600; 
set(gcf,'position',[x03,y03,width3,height3])
xlabel('Sequence 1'); 
ylabel('Sequence 2'); 
title('Matrix Filtered'); 
saveas(gcf,'Dot_Plot_Filtered.png')
toc

%-- END ALTERNATIVE FROM 

Finish = true; 